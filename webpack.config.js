const HtmlWebpackPlugin = require("html-webpack-plugin");
const path = require("path");


const htmlWebpackPlugin = new HtmlWebpackPlugin({
  template: "./src/client/index.html",
  filename: "./index.html",
});

module.exports = {
  entry: [
    "./src/client/index.js",
  ],
  output: {
    path: path.resolve(__dirname, "dist"),
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            presets: [
              "@babel/preset-env",
              "@babel/preset-react"
            ],
            plugins: [
              "@babel/plugin-proposal-class-properties",
            ],
          },
        },
      },
      {
        test: /\.scss$/,
        exclude: /node_modules/,
        use: [
          "style-loader",
          "css-loader",
          "sass-loader",
        ],
      },
    ],
  },
  plugins: [
    htmlWebpackPlugin,
  ],
  devServer: {
    port: 8000,
    proxy: {
      "/api": "http://localhost:8080",
    },
  },
};
