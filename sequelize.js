const path = require("path");

if (process.env.NODE_ENV === "development") {
  require("dotenv").config({
    path: path.resolve(__dirname, ".env"),
  });
}


module.exports = {
  development: {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    dialect: process.env.DB_DIALECT,
    database: process.env.DB_DATABASE,
  },
  production: {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    dialect: process.env.DB_DIALECT,
    database: process.env.DB_DATABASE,
    dialectOptions: {
      ssl: true,
    },
  },
};
