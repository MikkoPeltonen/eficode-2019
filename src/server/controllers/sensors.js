const db = require("../models");


class SensorController {
  static getSensors(req, res) {
    db.Sensor.findAll({
      attributes: [
        "code",
        "name",
      ],
    }).then(sensors => {
      return res.json(sensors);
    }).catch(() => {
      return res.sendStatus(500);
    });
  }

  static createSensor(req, res) {
    const name = req.body.name;
    const code = req.body.code;

    if (name === undefined || code === undefined) {
      return res.sendStatus(400);
    }

    db.Sensor.create({
      name,
      code,
    }).then(sensor => {
      return res.status(201).json(sensor);
    }).catch(db.Sequelize.UniqueConstraintError, () => {
      return res.status(409).send("Sensor already exists!");
    }).catch((e) => {
      return res.sendStatus(500);
    });
  }
}

module.exports = SensorController;
