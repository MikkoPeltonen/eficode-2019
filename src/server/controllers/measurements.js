const axios = require("axios");
const _ = require("lodash");

const db = require("../models");


class MeasurementController {
  static getMeasurements(req, res) {
    db.Sensor.findAll({
      attributes: [
        "name",
      ],
      order: [
        ["name", "ASC"],
        [{ model: db.Measurement, as: "measurements" }, "takenOn", "ASC"],
      ],
      include: [{
        model: db.Measurement,
        as: "measurements",
        attributes: [
          "value",
          "takenOn",
        ],
      }],
    }).then(sensors => {
      return res.json(sensors);
    }).catch(() => {
      return res.sendStatus(500);
    });
  }

  static loadMeasurements(req, res) {
    axios.get(process.env.API_URL + "/events", {
      headers: {
        Authorization: `Bearer ${process.env.API_ACCESS_TOKEN}`,
      },
    }).then(response => {
      const data = response.data;
      const measurements = _.omit(data, "date");
      const takenOn = data.date;

      db.Sensor.findAll({
        attributes: [
          "id",
          "code",
        ],
        where: {
          code: {
            [db.Sequelize.Op.in]: Object.keys(measurements),
          },
        },
        raw: true,
      }).then(sensors => {
        const validSensors = {};
        for (const sensor of sensors) {
          validSensors[sensor.code] = sensor.id;
        }

        // Pick only measurements for sensors already in the database
        const filteredMeasurements = _.pick(measurements, Object.keys(validSensors));
        const measurementObjects = Object.entries(filteredMeasurements).map(([sensorCode, value]) => {
          return {
            value,
            takenOn,
            SensorId: validSensors[sensorCode],
          };
        });

        db.Measurement.bulkCreate(measurementObjects).then(() => {
          return res.sendStatus(201);
        }).catch((e) => {
          if (e instanceof db.Sequelize.UniqueConstraintError) {
            return res.status(409).send("Database is already up to date!");
          }

          return res.sendStatus(500);
        })
      }).catch(() => {
        res.sendStatus(500);
      });
    }).catch(() => {
      return res.sendStatus(500);
    });
  }
}

module.exports = MeasurementController;
