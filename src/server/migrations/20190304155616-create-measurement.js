"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable("Measurements", {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
      },
      SensorId: {
        type: Sequelize.INTEGER,
        references: {
          model: "Sensors",
          key: "id",
        },
      },
      value: {
        type: Sequelize.DECIMAL,
        allowNull: true,
      },
      takenOn: {
        type: Sequelize.DATE,
        allowNull: false,
      },
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable("Measurements");
  },
};
