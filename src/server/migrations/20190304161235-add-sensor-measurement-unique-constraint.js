"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addConstraint("Measurements", ["SensorId", "takenOn"], {
      type: "unique",
      name: "sensor_datetime_unique",
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeConstraint("Measurements", "sensor_datetime_unique");
  }
};
