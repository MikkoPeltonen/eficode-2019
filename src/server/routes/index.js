const express = require("express");
const path = require("path");

const api = require("./api");


const router = express.Router();

router.use("/api", api);

router.get("*", function(req, res) {
  res.sendFile(path.resolve(__dirname, "../../../dist/index.html"));
});

module.exports = router;
