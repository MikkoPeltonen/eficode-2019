const express = require("express");

const SensorController = require("../controllers/sensors");
const MeasurementController = require("../controllers/measurements");


const router = express.Router();

router.get("/sensors", SensorController.getSensors);
router.post("/sensors", SensorController.createSensor);

router.get("/measurements", MeasurementController.getMeasurements);
router.patch("/measurements", MeasurementController.loadMeasurements);

module.exports = router;
