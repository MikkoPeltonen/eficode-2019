const Sequelize = require("sequelize");
const path = require("path");
const fs = require("fs");


const sequelize = new Sequelize(process.env.DB_DATABASE, process.env.DB_USER, process.env.DB_PASSWORD, {
  host: process.env.DB_HOST,
  dialect: process.env.DB_DIALECT,
  port: process.env.DB_PORT,
  logging: process.env.NODE_ENV === "development" ? console.log : false,
  dialectOptions: {
    ssl: process.env.NODE_ENV === "production",
  },
});

const db = {};

// Get all models from the current directory
fs.readdirSync(__dirname)
  .filter(file => !!file.indexOf(".") && file !== "index.js")
  .forEach((file) => {
    const model = sequelize.import(path.join(__dirname, file));
    db[model.name] = model;
  });

// Make associations for each model
Object.keys(db).forEach((modelName) => {
  if ("associate" in db[modelName]) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

db.sequelize.sync();

// Newer Sequelize versions running on Postgres return numbers are strings to retain precision
db.Sequelize.postgres.DECIMAL.parse = (value) => parseFloat(value);
db.Sequelize.postgres.INTEGER.parse = (value) => parseInt(value);
db.Sequelize.postgres.BIGINT.parse = (value) => parseInt(value);

module.exports = db;
