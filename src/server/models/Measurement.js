module.exports = (sequelize, DataTypes) => {
  const Measurement = sequelize.define("Measurement", {
    value: {
      type: DataTypes.DECIMAL,
      allowNull: false,
    },
    takenOn: {
      type: DataTypes.DATE,
      allowNull: false,
    },
  }, {
    timestamps: false,
  });

  Measurement.associate = (models) => {
    Measurement.belongsTo(models.Sensor);
  };

  return Measurement;
};
