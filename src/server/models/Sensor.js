module.exports = (sequelize, DataTypes) => {
  const Sensor = sequelize.define("Sensor", {
    name: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false,
    },
    code: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false,
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false,
    },
  });

  Sensor.associate = (models) => {
    Sensor.hasMany(models.Measurement, { as: "measurements" });
  };

  return Sensor;
};
