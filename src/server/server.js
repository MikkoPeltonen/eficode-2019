const express = require("express");
const bodyParser = require("body-parser");
const path = require("path");

if (process.env.NODE_ENV === "development") {
  require("dotenv").config({
    path: path.resolve(__dirname, "../../.env"),
  });
}

const routes = require("./routes");


const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(express.static(path.resolve(__dirname, "../../dist")));

app.use("/", routes);

app.listen(process.env.PORT || 8080, () => {});
