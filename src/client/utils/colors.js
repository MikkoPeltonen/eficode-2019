const colors = [
  "#1abc9c",
  "#f1c40f",
  "#9b59b6",
  "#e74c3c",
  "#2ecc71",
  "#3498db",
  "#e67e22",
];

export { colors };
