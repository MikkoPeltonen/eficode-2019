import axios from "axios";

class API {
  constructor() {
    this.url = "/api";
  }

  getSensors() {
    return axios.get(this.url + "/sensors").then(result => {
      return new Promise((resolve, reject) => resolve(result.data));
    });
  }

  getMeasurements() {
    return axios.get(this.url + "/measurements").then(result => {
      return new Promise((resolve, reject) => resolve(result.data));
    });
  }
}

export default API;
