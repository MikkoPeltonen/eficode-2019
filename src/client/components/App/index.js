import React from "react";

import API from "../../api";
import SensorGraph from "../SensorGraph";
import { colors } from "../../utils/colors";

import "./styles.scss";


const api = new API();

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      sensorData: [],
    };
  }

  componentDidMount() {
    api.getMeasurements().then(sensorData => {
      this.setState({
        sensorData,
      });
    }).catch(() => {});
  }

  render() {
    return (
      <div className="container">
        {this.state.sensorData.map((sensor, i) => { return (
          <div className="row graph-container" key={i}>
            <div className="col-12">
              <SensorGraph sensor={sensor} color={colors[i % colors.length]} />
            </div>
          </div>
        )})}
      </div>
    );
  }
}

export default App;
