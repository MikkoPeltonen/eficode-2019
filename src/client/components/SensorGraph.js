import React from "react";
import PropTypes from "prop-types";
import { Line } from "react-chartjs-2"


class SensorGraph extends React.Component {
  static propTypes = {
    sensor: PropTypes.object.isRequired,
    color: PropTypes.string.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      options: {
        responsive: true,
        scales: {
          xAxes: [{
            type: "time",
            scaleLabel: {
              display: true,
              labelString: "taken on",
            },
          }],
          yAxes: [{
            scaleLabel: {
              display: true,
              labelString: "sensor value",
            },
          }],
        },
        tooltips: {
          mode: "index",
          intersect: false,
        },
        legend: {
          display: false,
        },
        title: {
          display: true,
          text: "Sensor values over time",
          fontSize: 16,
        },
      },
    };
  }

  componentWillMount() {
    this.setState({
      options: {
        ...this.state.options,
        title: {
          ...this.state.options.title,
          text: `${this.props.sensor.name} measurements`,
        }
      }
    })
  }

  render() {
    const datasets = [{
      label: this.props.sensor.name,
      fill: false,
      borderColor: this.props.color,
      data: this.props.sensor.measurements.map(measurement => {
        return {
          x: new Date(measurement.takenOn),
          y: measurement.value,
        };
      }),
    }];

    return (
      <Line data={{ datasets }} options={this.state.options} />
    );
  }
}

export default SensorGraph;
